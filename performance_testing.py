"""
This is not the main code, it is just some experiments to see what is fast
"""


import timeit

setup = """
from operator import add,truediv

BLACK = 1
UNKNOWN = 0
WHITE = 2
truth_table_tuple = ((UNKNOWN, UNKNOWN, UNKNOWN), (UNKNOWN, BLACK, UNKNOWN), (UNKNOWN, UNKNOWN, WHITE))
truth_table_dict = {
    UNKNOWN: {UNKNOWN: UNKNOWN, BLACK: UNKNOWN, WHITE: UNKNOWN},
    BLACK: {UNKNOWN: UNKNOWN, BLACK: BLACK, WHITE: UNKNOWN},
    WHITE: {UNKNOWN: UNKNOWN, BLACK: UNKNOWN, WHITE: WHITE},
}
solution1 = (BLACK,)*10
solution2 = (WHITE,)*10
def f1(x,y):
    if not ((x - y) % 3):
        return x
    return 0
def f2(x,y):
    if ((x - y) % 3) == 0:
        return x
    return 0
def f3(x,y):
    if x == y:
        return x
    return 0
def f4(x,y):
    return x if x == y else 0
def f5(x,y):
    return truth_table_tuple[x][y]
def f6(x,y):
    return truth_table_dict[x][y]
"""

creation_fragments = [
    "(BLACK,)*10",
    "[BLACK]*10",
    "[BLACK for _ in range(10)]",
    "tuple([BLACK for _ in range(10)])",
    "tuple(BLACK for _ in range(10))",
]
concat_fragments = ["solution1+solution2", "(*solution1, *solution2)"]

combining_fragments = [
    "map(add, solution1, solution2)",
    "map(f1, solution1, solution2)",
    "map(f2, solution1, solution2)",
    "map(f3, solution1, solution2)",
    "map(f4, solution1, solution2)",
    "map(f5, solution1, solution2)",
    "map(f6, solution1, solution2)",
    "tuple(map(add, solution1, solution2))",
    "tuple(map(f1, solution1, solution2))",
    "tuple(map(f2, solution1, solution2))",
    "tuple(map(f3, solution1, solution2))",
    "tuple(map(f4, solution1, solution2))",
    "tuple(map(f5, solution1, solution2))",
    "tuple(map(f6, solution1, solution2))",
    "[cell if cell == solution2[i] else 0 for i, cell in enumerate(solution1)]",
    "[truth_table_tuple[cell][solution2[i]] for i, cell in enumerate(solution1)]",
    "tuple([cell + solution2[i] for i, cell in enumerate(solution1)])",
    "tuple([solution1[i] + solution2[i] for i in range(len(solution1))])",
]

runtimes = [
    (
        timeit.timeit(
            fragment,
            setup=setup,
            number=1_000_000,
        ),
        fragment,
    )
    for fragment in combining_fragments
]
runtimes.sort(key=lambda x: x[0])
print("times are in seconds for 1_000_000 iterations")
print("runtime\t\tfragment")
for runtime, fragment in runtimes:
    print(f"{runtime:.4f}\t\t{fragment}")
