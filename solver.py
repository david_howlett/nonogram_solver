"""
This is a nonogram solver written by David Howlett for fun during
Christmas 2023. The puzzle format is defined by
https://ssjools.hopto.org/nonogram/fmt2

I record the times for solving the brain bashers weekly special:
606.793 milliseconds first attempt
36.512 milliseconds memoize solve_fragment
26.747 milliseconds memoize solve_line
99.286 milliseconds rewrote solver to recursively divide lines in two
838.455 milliseconds ran all fragments through multiple solvers to find logic errors
17.249 milliseconds switched to only running the left to right solver and did micro optimisations with timeit
16.763 milliseconds quit early on solved lines
15.832 milliseconds solve only the unsolved portion of the lines
"""
import functools
import itertools
import json
import time
from operator import add
from typing import List, Set, Tuple

BLACK = 1
UNKNOWN = 0
WHITE = -1
fragment_solutions: Set[Tuple[Tuple[int, ...], Tuple[int, ...], Tuple[int, Tuple[int, ...]]]] = set()


# This should be thrown when the situation can't be solved
class NoSolutions(Exception):
    pass


def load_puzzle(file: str) -> Tuple[List[List[int]], Tuple[Tuple[int, ...], ...], Tuple[Tuple[int, ...], ...]]:
    puzzle_file = open(file, encoding="UTF-8")
    text, width_str = puzzle_file.readline().split(" ")
    assert text == "width"
    width = int(width_str)
    text, height_str = puzzle_file.readline().split(" ")
    assert text == "height"
    height = int(height_str)
    assert puzzle_file.readline() == "\n"
    assert puzzle_file.readline() == "rows\n"
    rows = tuple(tuple(int(length) for length in puzzle_file.readline().split(",")) for _ in range(height))
    assert puzzle_file.readline() == "\n"
    assert puzzle_file.readline() == "columns\n"
    columns = tuple(tuple(int(length) for length in puzzle_file.readline().split(",")) for _ in range(width))
    # grid is stored in the format grid[row][column]
    grid = [[UNKNOWN] * len(columns) for _ in rows]
    return grid, rows, columns


def pretty_print_grid(grid):
    print("grid")
    for row in grid:
        print("  ".join("#" if cell == 1 else ("." if cell == -1 else "?") for cell in row))


def split_blocks(blocks: Tuple[int, ...]) -> Tuple[Tuple[int, ...], int, Tuple[int, ...]]:
    split_point = len(blocks) // 2
    return blocks[:split_point], blocks[split_point], blocks[split_point + 1 :]


def get_positions_to_explore(
    fragment: Tuple[int, ...], left_blocks: Tuple[int, ...], block: int, right_blocks: Tuple[int, ...]
):
    """
    Given a line fragment and a block split in three, this calculates a collection of positions for the center block.
    The positions include all validate positions for the block and a few more that we don't know enough to exclude.
    Trimming possibilities at this stage makes the program much faster as it trims branches of the search tree.
    """
    length = len(fragment)
    if left_blocks:
        # this constraint is that there must be enough space on the left of the block for all the left_blocks to fit
        left_space_constraint = sum(left_blocks) + len(left_blocks)
        # if there are left blocks then the right_black_constraint is trivial
        right_black_constraint = length
    else:
        # if there are no blocks on the left then the left space constraint is trivial
        left_space_constraint = 0
        try:
            # todo this line is ugly
            # if there are no left_blocks then we are not allowed any black cells on the left of the
            # block we are placing
            right_black_constraint = length - fragment[::-1].index(BLACK)
        except ValueError:
            right_black_constraint = length
    if right_blocks:
        # this constraint is that there must be enough space on the right of the block for all the right_blocks to fit
        right_space_constraint = length - block + 1 - sum(right_blocks) - len(right_blocks)
        # if there are right blocks then the left_black_constraint is trivial
        left_black_constraint = 0
    else:
        # if there are no blocks on the right then the right space constraint is trivial
        right_space_constraint = length - block + 1
        try:
            first_black_block = fragment.index(BLACK)  # todo handle there being no blacks and index failing
            # if there are no right_blocks then we are not allowed any black cells on the right of the
            # block we are placing
            left_black_constraint = first_black_block - block
        except ValueError:
            left_black_constraint = 0
    leftmost_block_boundary = max(left_space_constraint, left_black_constraint)
    rightmost_block_boundary = min(right_space_constraint, right_black_constraint)
    valid_positions = tuple(
        position
        for position in range(leftmost_block_boundary, rightmost_block_boundary)
        # check for solution being invalid due to there being any white cells where we are placing a block
        if WHITE not in fragment[position : position + block]
        # correct block locations can't have a black square just before
        and (position == 0 or fragment[position - 1] != BLACK)
        # correct block locations can't have a black square just after
        and (position + block == length or fragment[position + block] != BLACK)
    )
    return valid_positions


@functools.lru_cache
def solve_fragment(fragment: Tuple[int, ...], blocks: Tuple[int, ...]) -> Tuple[int, Tuple[int, ...]]:
    """This runs all solvers and records the results"""
    solution1 = solve_fragment_by_splitting(fragment, blocks)
    solution2 = solve_fragment_left_to_right(fragment, blocks)
    assert solution1 == solution2
    if len(fragment) < 8:
        solution3 = solve_fragment_by_brute_force(fragment, blocks)
        assert solution1 == solution3
    fragment_solutions.add((fragment, blocks, solution1))
    return solution1


@functools.lru_cache
def solve_fragment_by_brute_force(fragment: Tuple[int, ...], blocks: Tuple[int, ...]) -> Tuple[int, Tuple[int, ...]]:
    """
    This solves a fragment by trying every possibility in a stupid way.
    I do this as a check on the faster but harder to follow logic.
    """
    all_solutions = list(itertools.product([BLACK, WHITE], repeat=len(fragment)))
    possible_solutions = []
    for solution in all_solutions:
        solution_blocks: List[int] = []
        in_block = False
        for i, cell in enumerate(solution):
            if -cell == fragment[i]:
                break
            if cell == 1:
                if in_block:
                    solution_blocks[-1] += 1
                else:
                    solution_blocks.append(1)
                    in_block = True
            else:
                in_block = False
        else:
            if blocks == tuple(solution_blocks):
                possible_solutions.append(solution)
    if not possible_solutions:
        raise NoSolutions
    combined_solution = tuple(sum(solution[i] for solution in possible_solutions) for i in range(len(fragment)))
    return len(possible_solutions), combined_solution


@functools.lru_cache
def solve_fragment_by_splitting(fragment: Tuple[int, ...], blocks: Tuple[int, ...]) -> Tuple[int, Tuple[int, ...]]:
    """
    This function recursively solves a fragment by picking the middle block and trying all locations it could be.
    This divides the fragment into left and right pieces. These are then solved recursively.
    """
    # if len(blocks) < 5:
    #    return solve_fragment_left_to_right(fragment, blocks)
    # To save on compute, I want to stop the recursion before calling with empty fragments or blocks
    assert fragment
    assert blocks

    length = len(fragment)
    minimum_space_for_blocks = sum(blocks) + len(blocks) - 1
    # If this function is called with insufficient space to place blocks then there is a logical error
    assert length >= minimum_space_for_blocks

    left_blocks, block, right_blocks = split_blocks(blocks)
    valid_positions = get_positions_to_explore(fragment, left_blocks, block, right_blocks)
    solution = (UNKNOWN,) * length
    solution_count = 0
    for position in valid_positions:
        try:
            # blocks touching the far left don't need white squares on their left
            if position == 0:
                left_solution_count = 1
                left_solution: Tuple[int, ...] = ()
            # blocks one away from the far left need a white square but no recursion
            elif position == 1:
                if fragment[0] == BLACK:
                    raise NoSolutions
                left_solution_count = 1
                left_solution = (WHITE,)
            else:
                left_fragment = fragment[: position - 1]
                if left_blocks:
                    left_solution_count, left_solution = solve_fragment_by_splitting(left_fragment, left_blocks)
                    left_solution = left_solution + (left_solution_count * WHITE,)
                else:
                    if BLACK in left_fragment:
                        raise NoSolutions
                    left_solution_count = 1
                    left_solution = (WHITE,) * position
            # blocks touching the far right don't need white squares on their right
            if position + block == length:
                right_solution_count = 1
                right_solution: Tuple[int, ...] = ()
            # blocks one away from the far right need a white square but no recursion
            elif position + block + 1 == length:
                if fragment[-1] == BLACK:
                    raise NoSolutions
                right_solution_count = 1
                right_solution = (WHITE,)
            else:
                right_fragment = fragment[position + block + 1 :]
                if right_blocks:
                    right_solution_count, right_solution = solve_fragment_by_splitting(right_fragment, right_blocks)
                    right_solution = (right_solution_count * WHITE,) + right_solution
                else:
                    if BLACK in right_fragment:
                        raise NoSolutions
                    right_solution_count = 1
                    right_solution = (WHITE,) * (length - block - position)
            position_solution_count = left_solution_count * right_solution_count
            # todo this makes more tuples than needed, change after debugging
            position_solution = (
                tuple(cell * right_solution_count for cell in left_solution)
                + (BLACK * position_solution_count,) * block
                + tuple(cell * left_solution_count for cell in right_solution)
            )
            solution_count += position_solution_count
            solution = tuple(cell + position_solution[i] for i, cell in enumerate(solution))
        except NoSolutions:
            pass
    assert len(solution) == length
    if not solution_count:
        raise NoSolutions
    return solution_count, solution


# todo break this function into pieces
@functools.lru_cache
def solve_fragment_left_to_right(fragment: Tuple[int, ...], blocks: Tuple[int, ...]) -> Tuple[int, Tuple[int, ...]]:
    """
    This function recursively solves a fragment by attempting a small part of the solution and recursing.
    """
    # To save on compute, I want to stop the recursion before calling with empty fragments or blocks
    assert fragment
    assert blocks

    length = len(fragment)
    minimum_space_for_blocks = sum(blocks) + len(blocks) - 1
    # If this function is called with insufficient space to place blocks then there is a logical error
    assert length >= minimum_space_for_blocks

    block = blocks[0]
    # if we got to the end of the line, there is only one solution possible
    if length == block:
        if WHITE in fragment:
            raise NoSolutions
        solution_count = 1
        solution = (BLACK,) * length
    # If we haven't got to the end of the line, there are two possibilities to check
    #  1) a lone white square
    #  2) a block followed by a white square
    else:
        # check if it is valid to place a white square on the far left
        if length > minimum_space_for_blocks and fragment[0] != BLACK:
            try:
                solution_count, solutions_with_white = solve_fragment_left_to_right(fragment[1:], blocks)
                solution = (WHITE * solution_count,) + solutions_with_white
            except NoSolutions:
                solution_count = 0
                solution = (0,) * length
        else:
            solution_count = 0
            solution = (0,) * length
        # check if it is valid to place a block followed by a white square
        if WHITE not in fragment[:block] and fragment[block] != BLACK:
            # check if placing a block followed by a white square takes me to the end
            if length == block + 1:
                solution_count += 1
                solutions_with_block = (BLACK,) * block + (WHITE,)
                solution = tuple(map(add, solution, solutions_with_block))
            # check if placing the last block
            elif len(blocks) == 1:
                # if the last block is being placed, we need to check the rest of the line for blacks
                # if there are any blacks then there are no more solutions and we should do nothing
                if BLACK not in fragment[block:]:
                    solution_count += 1
                    solutions_with_block = (BLACK,) * block + (WHITE,) * (length - block)
                    solution = tuple(map(add, solution, solutions_with_block))
            else:
                try:
                    solution_count_with_block, solutions_with_block = solve_fragment_left_to_right(
                        fragment[block + 1 :], blocks[1:]
                    )
                    # there may be previously calculated solutions that we need to add to
                    solution_count += solution_count_with_block
                    solutions_with_block = (
                        (BLACK * solution_count_with_block,) * block
                        + (WHITE * solution_count_with_block,)
                        + solutions_with_block
                    )
                    solution = tuple(map(add, solution, solutions_with_block))
                except NoSolutions:
                    pass
    assert len(solution) == length
    if not solution_count:
        raise NoSolutions
    return solution_count, solution


@functools.lru_cache
def solve_line(line: Tuple[int, ...], blocks: Tuple[int, ...]) -> Tuple[int, ...]:
    """
    Takes a line (row or column) and the blocks to go in that line then
    modifies the line to hopefully be closer to the solution.
    """
    if UNKNOWN not in line:
        return line
    line_start = 0
    blocks_start = 0
    while True:
        if line[line_start] == WHITE:
            line_start += 1
        else:
            block = blocks[0]
            if sum(line[line_start : line_start + block]) == block and line[line_start + block] == WHITE:
                line_start += blocks[0] + 1
                blocks_start += 1
            else:
                break
    line_end = len(line) - 1
    blocks_end = len(blocks)
    while True:
        if line[line_end] == WHITE:
            line_end -= 1
        else:
            block = blocks[0]
            if sum(line[line_end : line_end - block]) == block and line[line_end - block] == WHITE:
                line_end -= blocks[0] + 1
                blocks_end -= 1
            else:
                break
    solution_count, solution = solve_fragment_left_to_right(
        line[line_start : line_end + 1], blocks[blocks_start:blocks_end]
    )
    improved_line = (
        line[:line_start]
        + tuple(
            [BLACK if cell == solution_count else WHITE if cell == -solution_count else UNKNOWN for cell in solution]
        )
        + line[line_end + 1 :]
    )
    return improved_line


def solve_puzzle(grid: List[List[int]], rows: Tuple[Tuple[int, ...], ...], columns: Tuple[Tuple[int, ...], ...]):
    while not all(cell for z in grid for cell in z):
        # the row constraint is easier to work with
        for row, blocks in enumerate(rows):
            line = tuple(grid[row])
            improved_line = solve_line(line, blocks)
            for column, cell in enumerate(improved_line):
                grid[row][column] = cell
        # pretty_print_grid(grid)

        # the column constraint requires a bit more work, there might be a better abstraction possible
        for column, blocks in enumerate(columns):
            line = tuple(row[column] for row in grid)
            improved_line = solve_line(line, blocks)
            for row, cell in enumerate(improved_line):
                grid[row][column] = cell
        # pretty_print_grid(grid)
    return grid


def main():
    """
    This can be improved later with a tree search when we get stuck.
    """
    for filename in ["easy_problem.txt", "brain_bashers_weekly_special.txt"]:
        grid, rows, columns = load_puzzle(filename)
        start_time = time.perf_counter()
        solution = solve_puzzle(grid, rows, columns)
        pretty_print_grid(solution)
        print(f"The puzzle was solved in {1000 * (time.perf_counter() - start_time):.3f} milliseconds")
    if fragment_solutions:
        fragment_solutions_list = list(fragment_solutions)
        fragment_solutions_list.sort(
            key=lambda problem: len(problem[0]) + 0.01 * len(problem[1]) + 0.0001 * sum(problem[0])
        )
        test_cases = "[\n  " + ",\n  ".join(json.dumps(solution) for solution in fragment_solutions_list) + "\n]"
        open("fragment_test_cases.json", "w", encoding="utf-8").write(test_cases)


if __name__ == "__main__":
    main()
