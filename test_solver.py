"""
Tests the code in solver
"""

import pytest

import solver
from solver import BLACK, UNKNOWN, WHITE, NoSolutions

fast_solvers = [solver.solve_fragment_left_to_right, solver.solve_fragment_by_splitting]
solvers = fast_solvers + [solver.solve_fragment_by_brute_force]


def test_split_blocks():
    assert solver.split_blocks((1,)) == ((), 1, ())
    assert solver.split_blocks((5, 6)) == ((5,), 6, ())
    assert solver.split_blocks((1, 2, 3)) == ((1,), 2, (3,))
    assert solver.split_blocks((1, 2, 3, 4, 5)) == ((1, 2), 3, (4, 5))


def test_get_valid_positions():
    assert solver.get_positions_to_explore((UNKNOWN,), (), 1, ()) == (0,)
    assert solver.get_positions_to_explore((BLACK,), (), 1, ()) == (0,)
    assert not solver.get_positions_to_explore((WHITE,), (), 1, ())
    assert not solver.get_positions_to_explore((WHITE, UNKNOWN, UNKNOWN), (), 3, ())
    assert solver.get_positions_to_explore((UNKNOWN, BLACK, UNKNOWN), (), 3, ()) == (0,)
    assert solver.get_positions_to_explore((UNKNOWN, BLACK, UNKNOWN), (), 1, ()) == (1,)
    assert not solver.get_positions_to_explore((UNKNOWN, UNKNOWN, WHITE), (), 3, ())
    assert solver.get_positions_to_explore((UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN), (1,), 1, ()) == (2, 3)
    assert not solver.get_positions_to_explore((UNKNOWN, UNKNOWN, WHITE, UNKNOWN, UNKNOWN), (), 3, ())
    assert solver.get_positions_to_explore((UNKNOWN, UNKNOWN, WHITE, UNKNOWN, BLACK), (), 2, ()) == (3,)
    # this would be nice logic to have but it is not implemented yet
    # assert solver.get_positions_to_explore((BLACK, UNKNOWN, UNKNOWN, WHITE, BLACK), (1,), 1, ()) == (4,)
    assert solver.get_positions_to_explore(
        (
            WHITE,
            UNKNOWN,
            BLACK,
            UNKNOWN,
            UNKNOWN,
            WHITE,
        ),
        (),
        3,
        (),
    ) == (1, 2)
    assert solver.get_positions_to_explore(
        (
            UNKNOWN,
            WHITE,
            UNKNOWN,
            BLACK,
            WHITE,
            UNKNOWN,
            BLACK,
            UNKNOWN,
            BLACK,
            WHITE,
            UNKNOWN,
            UNKNOWN,
            WHITE,
            UNKNOWN,
            UNKNOWN,
        ),
        (1, 2),
        3,
        (2, 2),
    ) == (6,)
    assert solver.get_positions_to_explore(
        (
            BLACK,
            WHITE,
            UNKNOWN,
            UNKNOWN,
            WHITE,
            UNKNOWN,
            BLACK,
            WHITE,
            BLACK,
            WHITE,
            UNKNOWN,
            UNKNOWN,
            WHITE,
            UNKNOWN,
            UNKNOWN,
        ),
        (1, 2),
        1,
        (2, 2),
    ) == (6, 8)


@pytest.mark.parametrize("fragment_solver", solvers)
def test_solve_fragment_with_impossible_to_place_blocks(fragment_solver):
    with pytest.raises(NoSolutions):
        fragment_solver((WHITE, UNKNOWN), (2,))
    with pytest.raises(NoSolutions):
        fragment_solver((UNKNOWN, WHITE), (2,))
    with pytest.raises(NoSolutions):
        fragment_solver((UNKNOWN, WHITE, UNKNOWN), (2,))
    with pytest.raises(NoSolutions):
        fragment_solver((WHITE, WHITE), (2,))


@pytest.mark.parametrize("solve_fragment", solvers)
def test_solve_fragment_with_oneshot_solution(solve_fragment):
    assert solve_fragment((UNKNOWN,), (1,)) == (1, (BLACK,))
    assert solve_fragment((BLACK,), (1,)) == (1, (BLACK,))
    assert solve_fragment((UNKNOWN, UNKNOWN), (2,)) == (1, (BLACK, BLACK))
    assert solve_fragment((UNKNOWN, BLACK, WHITE), (2,)) == (1, (BLACK, BLACK, WHITE))
    assert solve_fragment((UNKNOWN, BLACK, UNKNOWN), (2,)) == (2, (UNKNOWN, 2, UNKNOWN))
    assert solve_fragment((UNKNOWN, BLACK, UNKNOWN), (3,)) == (1, (BLACK, BLACK, BLACK))
    assert solve_fragment((UNKNOWN, UNKNOWN, WHITE, UNKNOWN, BLACK), (2,)) == (
        1,
        (WHITE, WHITE, WHITE, BLACK, BLACK),
    )


@pytest.mark.parametrize("solve_fragment", solvers)
def test_solve_fragment_with_complex_problem(solve_fragment):
    assert solve_fragment((UNKNOWN, UNKNOWN, UNKNOWN), (2,)) == (2, (0, 2, 0))
    assert solve_fragment((UNKNOWN, BLACK, WHITE), (2,)) == (1, (1, 1, -1))
    assert solve_fragment((UNKNOWN, BLACK, WHITE, UNKNOWN), (2,)) == (1, (1, 1, -1, -1))
    assert solve_fragment((UNKNOWN, UNKNOWN, UNKNOWN, UNKNOWN), (1, 1)) == (3, (1, -1, -1, 1))
    assert solve_fragment((BLACK, UNKNOWN, UNKNOWN, WHITE, BLACK), (1, 1)) == (1, (1, -1, -1, -1, 1))
    assert solve_fragment((UNKNOWN, UNKNOWN, UNKNOWN, BLACK, UNKNOWN, UNKNOWN, UNKNOWN), (1, 1, 1)) == (
        4,
        (0, 0, -4, 4, -4, 0, 0),
    )
    assert solve_fragment((UNKNOWN, UNKNOWN, WHITE, UNKNOWN, BLACK, WHITE, UNKNOWN, UNKNOWN), (2, 2)) == (
        2,
        (0, 0, -2, 2, 2, -2, 0, 0),
    )


# @pytest.mark.parametrize("solve_fragment", fast_solvers)
# def test_solve_fragment_with_bulk_problems(solve_fragment):
#    for fragment, blocks, (solution_count, solution) in json.load(open("fragment_test_cases.json", encoding="utf-8")):
#        assert solve_fragment(tuple(fragment), tuple(blocks)) == (solution_count, tuple(solution))


def test_solve_line():
    assert solver.solve_line((UNKNOWN, UNKNOWN, WHITE, UNKNOWN, BLACK, WHITE, UNKNOWN, UNKNOWN), (2, 2)) == (
        UNKNOWN,
        UNKNOWN,
        WHITE,
        BLACK,
        BLACK,
        WHITE,
        UNKNOWN,
        UNKNOWN,
    )
    assert solver.solve_line((1, 0, -1, -1, 1), (1, 1)) == (
        BLACK,
        WHITE,
        WHITE,
        WHITE,
        BLACK,
    )
